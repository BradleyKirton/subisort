"""Configurable sublime text isort plugin."""

import logging
import pathlib
import subprocess

import sublime
import sublime_plugin

PACKAGE_NAME = "subisort"
SETTINGS_FILE_NAME = "{}.sublime-settings".format(PACKAGE_NAME)
CONFIG = {"profile", "sort_on_save"}

logger = logging.getLogger(__file__)


def plugin_loaded():
    """Handler for plugin loaded event."""

    settings = load_settings()
    log_level = settings.get("log_level", "info")

    if not logger.handlers:
        debug_formatter = logging.Formatter(
            "[{}:%(filename)s](%(levelname)s) %(message)s".format(PACKAGE_NAME)
        )
        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(debug_formatter)
        logger.addHandler(handler)

    try:
        logger.setLevel(log_level.upper())
    except ValueError as e:
        logger.error(e)
        logger.setLevel(logging.ERROR)
        logger.error("fallback to loglevel ERROR")

    logger.info("Loglevel set to {}".format(log_level.upper()))

    # watch for loglevel change
    sublime.load_settings(SETTINGS_FILE_NAME).add_on_change(
        "log_level", lambda: pathlib.Path(__file__).touch()
    )


def load_settings():
    """Load the package settings."""

    def iter_settings():
        sublime_settings = sublime.load_settings(SETTINGS_FILE_NAME)
        for option in CONFIG:
            yield option, sublime_settings.get(option)

    return dict(iter_settings())


def is_python(view):
    """Returns True if the view is a python file."""

    return view.match_selector(0, "source.python")


def get_project_path(view):
    """Return the root folder."""

    window = view.window()
    folders = window.folders()

    if folders:
        return min(folders)
    else:
        return None


def find_settings_root_path(view):
    """Return the path to the settings path."""

    project_root = get_project_path(view=view)

    if project_root is None:
        logger.info("project root not found, aborting search for isort config.")
        return None

    project_root_path = pathlib.Path(project_root)
    current_file_name = view.file_name()
    current_file_path = pathlib.Path(current_file_name)

    def has_config_file(path, config_file):
        return next(path.glob(config_file), None) is not None

    for path in current_file_path.parents:
        conds = [
            has_config_file(path, ".isort.cfg"),
            has_config_file(path, "pyproject.toml"),
        ]

        if any(conds) is True:
            return str(path.absolute())

        if path == project_root_path:
            logger.info("isort config file not found, does  your project have one?")
            return None


def sort_code_string(content, settings_path=None, profile=None):
    """Sort the imports within the provided content."""

    command = ["isort", "--stdout", "-"]

    if settings_path:
        command.extend(["--settings-path", settings_path])

    if profile:
        command.extend(["--profile", profile])

    process = subprocess.Popen(
        args=command,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )

    return process.communicate(content.encode())


def sort_view(edit, view):
    """Sort the content within the provided view."""

    if is_python(view) is False:
        return None

    settings = load_settings()
    profile = settings.get("profile", "black")
    settings_path = find_settings_root_path(view=view)

    view_size = view.size()
    region = sublime.Region(0, view_size)
    unsorted_content = view.substr(region)
    output, error = sort_code_string(
        content=unsorted_content,
        settings_path=settings_path,
        profile=profile,
    )

    if error:
        logger.error(
            "isort command failed with the following error: {}".format(error.decode())
        )
        return None

    sorted_content = output.decode()
    current_positions = list(view.sel())

    view.replace(edit, region, sorted_content)

    # Reset cursor
    remove_sel = view.sel()[0]
    view.sel().subtract(remove_sel)

    for position in current_positions:
        view.sel().add(position)


class SortImportsCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        """Sort the view."""

        sort_view(edit, self.view)


class OnSaveHandler(sublime_plugin.ViewEventListener):
    def on_pre_save(self):
        """Sort the view on save."""

        settings = load_settings()
        sort_on_save = settings.get("sort_on_save", False)

        if sort_on_save is False:
            return None

        self.view.run_command("sort_imports")
