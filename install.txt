  ___  _   _ | |__  (_) ___   ___   _ __ | |_ 
 / __|| | | || '_ \ | |/ __| / _ \ | '__|| __|
 \__ \| |_| || |_) || |\__ \| (_) || |   | |_ 
 |___/ \__,_||_.__/ |_||___/ \___/ |_|    \__|

Thanks for installing subisort.

Before you can start using subisort be sure to install `isort` on your system. You can install `isort` using your package manager or why not try pipx (https://pipxproject.github.io/pipx/)?

Subisort is a configurable `isort` plugin which let's you specify:

- profile
- sort_on_save
- log_level

For more information on the `profile` see https://pycqa.github.io/isort/docs/configuration/profiles/.

Subisort tries to keep things intentionally simple. It will either make use of the profile you provide or settings contained within a pyproject.toml or .isort.cfg file. Subisort will attempt to file the config files mentioned above by searching your project.